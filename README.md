## This repo is under development

# Universis
[Universis](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

#### @universis/registrar

[Universis project registrar application](packages/registrar/)

#### @universis/theme

[Universis project theme for client applications](packages/theme/)

#### @universis/common

[Universis project common library](packages/common/)

## Build development environment

Clone repository:

    git clone https://gitlab.com/universis/universis.git
    
Install dependencies:

    cd universis
    npm i

(install peer dependencies also)
  
Install @universis/theme dependencies:

    cd packages/theme
    npm i
    
Install @universis/common dependencies:

    cd packages/common
    npm i

Go to project root and build @universis/common:

    ng build common
    
Clone app.json to app.development.json

    cp packages/registrar/src/assets/config/app.json packages/registrar/src/assets/config/app.development.json 
    
Serve registrar

    ng serve registrar

## Documentation

Follow [this link](https://universis.gitlab.io/universis/) to read more about the packages of this project.


    

