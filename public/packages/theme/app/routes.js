const routes = [
    {
        name: 'colors',
        title: 'Colors',
        url: '/colors',
        templateUrl: './app/colors.html'
    },
    {
        name: 'typography',
        title: 'Typography',
        url: '/typography',
        templateUrl: './app/typography.html'
    },
    {
        name: 'components',
        title: 'Components',
        url: '/components',
        templateUrl: './app/components.html',
        redirectTo: 'components.cards'
    },
    {
        name: 'components.cards',
        title: 'Cards',
        url: '/cards',
        templateUrl: './app/components/cards.html'
    },
    {
        name: 'components.buttons',
        title: 'Buttons',
        url: '/buttons',
        templateUrl: './app/components/buttons.html'
    },
    {
        name: 'forms',
        title: 'Forms',
        url: '/forms',
        templateUrl: './app/forms.html',
        redirectTo: 'forms.simple'
    },
    {
        name: 'forms.simple',
        title: 'Simple',
        url: '/simple',
        templateUrl: './app/forms/simple-form.html'
    }
];

module.exports = routes;
