
import 'angular';
import '@uirouter/angularjs';
import {Sidebar} from '@coreui/coreui';
import hljs from 'highlight.js/lib/highlight';
import prettier from 'prettier';
import htmlParser from 'prettier/parser-html';
import xmlLanguage from 'highlight.js/lib/languages/xml';
import javascriptLanguage from 'highlight.js/lib/languages/javascript';
import typescriptLanguage from 'highlight.js/lib/languages/typescript';
import routes from './routes.js';

hljs.registerLanguage('xml', xmlLanguage);
hljs.registerLanguage('javascript', javascriptLanguage);
hljs.registerLanguage('typescript', typescriptLanguage);

const main = angular.module('previewApp', ['ui.router']);

main.config(routeConfig);

function routeConfig($locationProvider, $stateProvider, $urlRouterProvider) {

    // router states
    routes.forEach( route => {
        $stateProvider.state(route);
    });
    $urlRouterProvider.otherwise(routes[0].name);
    $locationProvider.html5Mode(false);
}

main.directive('sidebar', function AppSidebarDirective($state) {
    return {
        restrict: 'C',
        controller: function($scope) {
            //
        },
        link: function link(scope, element, attrs) {
            const nav = element.find('ul.nav');
            const states = $state.get().filter( x=> !x.abstract);
            states.filter( state => {
                return state.name.indexOf('.') < 0;
            }).forEach ( state => {
                let children = states.filter( y => {
                    return new RegExp('^' + state.name + '.', 'ig').test(y.name);
                });

                if (Array.isArray(children) && children.length > 0) {
                    let dropdown = angular.element(`<li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="javascript:void(0)">${state.title}</a>
                        <ul class="nav-dropdown-items">
                    </ul>
                    </li>`);
                    let navItems = dropdown.find('.nav-dropdown-items');
                    children.forEach ( child => {
                        let href = $state.href(child);
                        navItems.append(`<li class="nav-item">
                            <a class="nav-link" href="${href}">
                                <i class="nav-icon fas fa-none"></i>
                                ${child.title}
                            </a>
                        </li>`);
                    });

                    nav.append(dropdown);
                }
                else {
                    let href = $state.href(state);
                    nav.append(`<li class="nav-item">
                        <a class="nav-link" href="${href}">
                            ${state.title}
                        </a>
                    </li>`);
                }
            });
            // initialize sidebar
            let sidebar = new Sidebar(element);

        }
    }
});

main.controller('ThemeColorController', ['$scope', function ThemeColorController($scope) {
    let vm = this;
    function activate() {

        function rgb2hex(rgb) {
            rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            function hex(x) {
                return ("0" + parseInt(x).toString(16)).slice(-2);
            }
            return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
        }

        vm.hexColor = function(selector) {
            return rgb2hex(angular.element(selector).css('background-color'));
        };
        vm.rgbColor = function(selector) {
            return angular.element(selector).css('background-color');
        };
    }
    activate();
}]);


main.directive('appHighlight', function AppHighlightDirective() {
    return {
        restrict: 'E',
        scope: {
          highlight:'=highlight'
        },
        template: `<div class="card">
                      <div class="card-body">
                        <pre><code class="hljs"></code></pre>
                      </div>
                    </div>
        `,
        link: function link(scope, element, attrs) {
            const vm = this;

            function escapeHTML(unsafe) {
                if (typeof unsafe == null) {
                    return;
                }
                return unsafe
                    .replace(/&/g, "&amp;")
                    .replace(/</g, "&lt;")
                    .replace(/>/g, "&gt;")
                    .replace(/"/g, "&quot;")
                    .replace(/'/g, "&#039;");
            }

            function activate() {
                if (scope.highlight) {
                    const targetElement = angular.element(scope.highlight);
                    if (targetElement) {
                        const codeElement = element.find('code').get(0);
                        // format html
                        const html = escapeHTML(prettier.format(targetElement.html(), {
                            parser: 'html',
                            printWidth: 140,
                            htmlWhitespaceSensitivity: 'ignore',
                            useTabs: false,
                            plugins: { parsers: htmlParser }
                        }));
                        if (codeElement) {
                            codeElement.innerHTML = html;
                            hljs.highlightBlock(codeElement);
                        }

                    }
                }
            }
            activate();
        }
    };
});
