import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsPreviewStudentsComponent } from './study-programs-preview-students.component';

describe('StudyProgramsPreviewStudentsComponent', () => {
  let component: StudyProgramsPreviewStudentsComponent;
  let fixture: ComponentFixture<StudyProgramsPreviewStudentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsPreviewStudentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsPreviewStudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
