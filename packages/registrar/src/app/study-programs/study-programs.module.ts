import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudyProgramsHomeComponent } from './components/study-programs-home/study-programs-home.component';
import {StudyProgramsRoutingModule} from './study-programs.routing';
import {StudyProgramsSharedModule} from './study-programs.shared';
import { StudyProgramsTableComponent } from './components/study-programs-table/study-programs-table.component';
import {TranslateModule} from '@ngx-translate/core';
import {TablesModule} from '../tables/tables.module';
import { StudyProgramsRootComponent } from './components/study-programs-root/study-programs-root.component';
import { StudyProgramsPreviewComponent } from './components/study-programs-preview/study-programs-preview.component';
import { StudyProgramsPreviewGeneralComponent } from './components/study-programs-preview/study-programs-preview-general/study-programs-preview-general.component';
import { StudyProgramsPreviewStudentsComponent } from './components/study-programs-preview/study-programs-preview-students/study-programs-preview-students.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    TablesModule,
    StudyProgramsSharedModule,
    StudyProgramsRoutingModule,
    SharedModule,
    FormsModule
  ],
  declarations: [StudyProgramsHomeComponent, StudyProgramsTableComponent, StudyProgramsRootComponent, StudyProgramsPreviewComponent, StudyProgramsPreviewGeneralComponent, StudyProgramsPreviewStudentsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StudyProgramsModule { }
