import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InternshipsHomeComponent } from './components/internships-home/internships-home.component';
import { InternshipsTableComponent } from './components/internships-table/internships-table.component';
import { InternshipsRoutingModule } from './internships.routing';
import {InternshipsSharedModule} from './internships.shared';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { InternshipsPreviewComponent } from './components/internships-preview/internships-preview.component';
import { InternshipsRootComponent } from './components/internships-root/internships-root.component';
import { InternshipsPreviewGeneralComponent } from './components/internships-preview-general/internships-preview-general.component';
import { InternshipsPreviewStudentsComponent } from './components/internships-preview-students/internships-preview-students.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    TablesModule,
    InternshipsRoutingModule,
    InternshipsSharedModule,
    SharedModule,
    FormsModule
  ],
  declarations: [InternshipsHomeComponent, InternshipsTableComponent, InternshipsPreviewComponent, InternshipsRootComponent, InternshipsPreviewGeneralComponent, InternshipsPreviewStudentsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class InternshipsModule {
  constructor(private _translateService: TranslateService){}
 }
