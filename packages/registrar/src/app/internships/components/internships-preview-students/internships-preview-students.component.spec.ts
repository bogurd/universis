import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternshipsPreviewStudentsComponent } from './internships-preview-students.component';

describe('InternshipsPreviewStudentsComponent', () => {
  let component: InternshipsPreviewStudentsComponent;
  let fixture: ComponentFixture<InternshipsPreviewStudentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternshipsPreviewStudentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternshipsPreviewStudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
