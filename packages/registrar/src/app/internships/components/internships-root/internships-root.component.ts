import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-internships-root',
  template: '<router-outlet></router-outlet>'
})
export class InternshipsRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
