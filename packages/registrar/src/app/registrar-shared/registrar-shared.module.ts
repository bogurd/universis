import {ModuleWithProviders, NgModule, OnInit, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AppSidebarService} from './services/app-sidebar.service';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';

@NgModule({
  imports: [
    CommonModule,
      TranslateModule
  ],
  declarations: [],
  providers: [
    AppSidebarService
  ]
})
export class RegistrarSharedModule implements OnInit {
  constructor(@Optional() @SkipSelf() parentModule: RegistrarSharedModule,
              private _translateService: TranslateService) {
    if (parentModule) {
      throw new Error(
          'RegistrarSharedModule is already loaded. Import it in the AppModule only');
    }
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading registrar shared module');
      console.error(err);
    });
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RegistrarSharedModule,
      providers: [
        AppSidebarService
      ]
    };
  }

  async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`../../assets/i18n/${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
  }

}
