import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScholarshipsHomeComponent } from './components/scholarships-home/scholarships-home.component';
import { ScholarshipsTableComponent } from './components/scholarships-table/scholarships-table.component';
import { ScholarshipsRoutingModule } from './scholarships.routing';
import {ScholarshipsSharedModule} from './scholarships.shared';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule} from '@ngx-translate/core';
import { ScholarshipsPreviewComponent } from './components/scholarships-preview/scholarships-preview.component';
import { ScholarshipsRootComponent } from './components/scholarships-root/scholarships-root.component';
import { ScholarshipsPreviewGeneralComponent } from './components/scholarships-preview/scholarships-preview-general/scholarships-preview-general.component';
import { ScholarshipsPreviewStudentsComponent } from './components/scholarships-preview/scholarships-preview-students/scholarships-preview-students.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    TablesModule,
    ScholarshipsRoutingModule,
    ScholarshipsSharedModule,
    SharedModule,
    FormsModule
  ],
  declarations: [ScholarshipsHomeComponent, ScholarshipsTableComponent, ScholarshipsPreviewComponent, ScholarshipsRootComponent, ScholarshipsPreviewGeneralComponent, ScholarshipsPreviewStudentsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ScholarshipsModule { }
