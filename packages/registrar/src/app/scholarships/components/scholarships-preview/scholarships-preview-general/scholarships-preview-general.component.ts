import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-scholarships-preview-general',
  templateUrl: './scholarships-preview-general.component.html'
})
export class ScholarshipsPreviewGeneralComponent implements OnInit {
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Scholarships')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('department')
      .getItem();
  }

}
