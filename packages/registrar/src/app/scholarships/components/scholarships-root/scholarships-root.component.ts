import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-scholarships-root',
  template: '<router-outlet></router-outlet>'

})
export class ScholarshipsRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
