import { Component, OnInit, ViewChild } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '../../../tables/components/advanced-table/advanced-table.component';
import * as STUDENTS_GRADES_LIST_CONFIG from './students-edit-grades.config.json';

@Component({
  selector: 'app-students-edit',
  templateUrl: './students-edit-grades.component.html',
  styleUrls: ['./students-edit.component.scss']
})
export class StudentsEditGradesComponent implements OnInit {
  public readonly config = STUDENTS_GRADES_LIST_CONFIG;

  @ViewChild('grades') grades: AdvancedTableComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.grades.query = this._context.model('StudentCourses')
      .where('student').equal(this._activatedRoute.snapshot.params.id)
      .expand('course')
      .prepare();
    this.grades.config = AdvancedTableConfiguration.cast(STUDENTS_GRADES_LIST_CONFIG);
  }

}
