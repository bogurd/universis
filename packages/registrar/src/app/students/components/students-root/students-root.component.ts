import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-root',
  templateUrl: './students-root.component.html',
  styleUrls: ['./students-root.component.scss']
})
export class StudentsRootComponent implements OnInit {
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Students')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('person,studyProgram')
      .getItem();


  }

}
