import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudentsHomeComponent} from './components/students-home/students-home.component';
import {StudentsTableComponent} from './components/students-table/students-table.component';
import {StudentsDashboardComponent} from './components/students-dashboard/students-dashboard.component';
import {StudentsEditComponent} from './components/students-edit/students-edit.component';
import {StudentsRootComponent} from './components/students-root/students-root.component';
import {StudentsEditGeneralComponent} from './components/students-edit/students-edit-general.component';
import {StudentsEditGradesComponent} from './components/students-edit/students-edit-grades.component';
import {StudentsEditThesesComponent} from './components/students-edit/students-edit-theses.component';


const routes: Routes = [
  {
    path: '',
    component: StudentsHomeComponent,
    data: {
      title: 'Students'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: StudentsTableComponent,
        data: {
          title: 'Students List'
        }
      }
    ]
  },
  {
    path: ':id',
    component: StudentsRootComponent,
    data: {
      title: 'Student Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: StudentsDashboardComponent,
        data: {
          title: 'Student Dashboard'
        }
      },
      {
        path: 'edit',
        component: StudentsEditComponent,
        data: {
          title: 'Student Edit'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'general'
          },
          {
            path: 'general',
            component: StudentsEditGeneralComponent,
            data: {
              title: 'Students Edit General'
            }
          },
          {
            path: 'grades',
            component: StudentsEditGradesComponent,
            data: {
              title: 'Students Edit Grades'
            }
          },
          {
            path: 'theses',
            component: StudentsEditThesesComponent,
            data: {
              title: 'Students Edit Theses'
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class StudentsRoutingModule {
}
