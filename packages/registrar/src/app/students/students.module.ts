import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentsHomeComponent } from './components/students-home/students-home.component';
import {StudentsRoutingModule} from './students.routing';
import {StudentsSharedModule} from './students.shared';
import { StudentsTableComponent } from './components/students-table/students-table.component';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule} from '@ngx-translate/core';
import { StudentsDashboardComponent } from './components/students-dashboard/students-dashboard.component';
import { StudentsEditComponent } from './components/students-edit/students-edit.component';
import { StudentsRootComponent } from './components/students-root/students-root.component';
import { StudentsEditGeneralComponent} from './components/students-edit/students-edit-general.component';
import { StudentsEditGradesComponent} from './components/students-edit/students-edit-grades.component';
import { StudentsEditThesesComponent} from './components/students-edit/students-edit-theses.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    StudentsSharedModule,
    TablesModule,
    StudentsRoutingModule,
    SharedModule,
    FormsModule
  ],
  declarations: [StudentsHomeComponent, StudentsTableComponent, StudentsDashboardComponent, StudentsEditComponent,
    StudentsEditGeneralComponent, StudentsEditGradesComponent, StudentsEditThesesComponent, StudentsRootComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StudentsModule { }
