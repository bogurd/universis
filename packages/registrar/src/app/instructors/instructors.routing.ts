import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InstructorsHomeComponent} from './components/instructors-home/instructors-home.component';
import {InstructorsTableComponent} from './components/instructors-table/instructors-table.component';
import {InstructorsEditComponent} from './components/instructors-edit/instructors-edit.component';
import {InstructorsDashboardComponent} from './components/instructors-dashboard/instructors-dashboard.component';
import {InstructorsRootComponent} from './components/instructors-root/instructors-root.component';
import {InstructorsEditGeneralComponent} from './components/instructors-edit/instructors-edit-general.component';
import {InstructorsEditClassesComponent} from './components/instructors-edit/instructors-edit-classes.component';
import {InstructorsEditExamsComponent} from './components/instructors-edit/instructors-edit-exams.component';

const routes: Routes = [
  {
    path: '',
    component: InstructorsHomeComponent,
    data: {
      title: 'Instructors'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: InstructorsTableComponent,
        data: {
          title: 'Instructors List'
        }
      }
    ]
  },
  {
    path: ':id',
    component: InstructorsRootComponent,
    data: {
      title: 'Instructor Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: InstructorsDashboardComponent,
        data: {
          title: 'Instructor Dashboard'
        }
      },
      {
        path: 'edit',
        component: InstructorsEditComponent,
        data: {
          title: 'Instructor Edit'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'general'
          },
          {
            path: 'general',
            component: InstructorsEditGeneralComponent,
            data: {
              title: 'Instructors Edit General'
            }
          },
          {
            path: 'classes',
            component: InstructorsEditClassesComponent,
            data: {
              title: 'Instructors Edit Classes'
            }
          },
          {
            path: 'exams',
            component: InstructorsEditExamsComponent,
            data: {
              title: 'Instructors Edit Exams'
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class InstructorsRoutingModule {
}
