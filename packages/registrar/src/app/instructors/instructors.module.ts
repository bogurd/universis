import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstructorsHomeComponent } from './components/instructors-home/instructors-home.component';
import {InstructorsRoutingModule} from './instructors.routing';
import {InstructorsSharedModule} from './instructors.shared';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import { InstructorsTableComponent } from './components/instructors-table/instructors-table.component';
import { InstructorsDashboardComponent } from './components/instructors-dashboard/instructors-dashboard.component';
import { InstructorsEditComponent } from './components/instructors-edit/instructors-edit.component';
import { InstructorsRootComponent } from './components/instructors-root/instructors-root.component';
import {InstructorsEditClassesComponent} from './components/instructors-edit/instructors-edit-classes.component';
import {InstructorsEditExamsComponent} from './components/instructors-edit/instructors-edit-exams.component';
import {InstructorsEditGeneralComponent} from './components/instructors-edit/instructors-edit-general.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';
import {ClasseSharedModule} from '../classes/classes.shared';
import {ExamsSharedModule} from '../exams/exams.shared';


@NgModule({
  imports: [
      CommonModule,
      TranslateModule,
      InstructorsSharedModule,
      InstructorsRoutingModule,
      ClasseSharedModule,
      ExamsSharedModule,
      TablesModule,
      SharedModule,
      FormsModule
  ],
  declarations: [InstructorsHomeComponent, InstructorsTableComponent, InstructorsDashboardComponent,
    InstructorsEditComponent, InstructorsRootComponent,
    InstructorsEditGeneralComponent, InstructorsEditExamsComponent, InstructorsEditClassesComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InstructorsModule {

    constructor(private _translateService: TranslateService) {
        //
    }

}
