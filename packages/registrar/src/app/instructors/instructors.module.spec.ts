import {TestBed, async, inject} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {InstructorsModule} from './instructors.module';
describe('InstructorsModule', () => {
  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot()
      ]
    }).compileComponents();
  }));
  it('should create an instance', inject([TranslateService], (translateService: TranslateService) => {
    const instructorsModule = new InstructorsModule(translateService);
    expect(instructorsModule).toBeTruthy();
  }));
});
