import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '../../../tables/components/advanced-table/advanced-table.component';
import {AngularDataContext} from '@themost/angular';
import * as INSTRUCTORS_CLASSES_LIST_CONFIG from './instructors-edit-classes.config.json';

@Component({
  selector: 'app-instructors-edit',
  templateUrl: './instructors-edit-classes.component.html',
  styleUrls: ['./instructors-edit.component.scss']
})
export class InstructorsEditClassesComponent implements OnInit {

  public readonly config = INSTRUCTORS_CLASSES_LIST_CONFIG;
  @ViewChild('classes') classes: AdvancedTableComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext
  ) { }

  async ngOnInit() {
    this.classes.query =  this._context.model('courseClassInstructors')
      .where('instructor').equal(this._activatedRoute.snapshot.params.id)
      .expand('courseClass($expand=course,year,period)')
      .prepare();
    this.classes.config = AdvancedTableConfiguration.cast(INSTRUCTORS_CLASSES_LIST_CONFIG);
  }

}
