import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-instructors-edit',
  templateUrl: './instructors-edit-general.component.html',
  styleUrls: ['./instructors-edit.component.scss']
})
export class InstructorsEditGeneralComponent implements OnInit {
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Instructors')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('department($expand=organization),user')
      .getItem();


  }
}
