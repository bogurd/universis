import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '../../../tables/components/advanced-table/advanced-table.component';
import {AngularDataContext} from '@themost/angular';
import * as INSTRUCTORS_EXAMS_LIST_CONFIG from './instructors-edit-exams.config.json';

@Component({
  selector: 'app-instructors-edit',
  templateUrl: './instructors-edit-exams.component.html',
  styleUrls: ['./instructors-edit.component.scss']
})
export class InstructorsEditExamsComponent implements OnInit {
  public readonly config = INSTRUCTORS_EXAMS_LIST_CONFIG;
  @ViewChild('exams') exams: AdvancedTableComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext)  { }

  async ngOnInit() {
    this.exams.query = this._context.model('courseExamInstructors')
      .where('instructor').equal(this._activatedRoute.snapshot.params.id)
      .expand('courseExam($expand=course,examPeriod)')
      .prepare();
    this.exams.config = AdvancedTableConfiguration.cast(INSTRUCTORS_EXAMS_LIST_CONFIG);
  }

}
