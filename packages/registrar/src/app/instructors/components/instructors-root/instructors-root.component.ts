import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-instructors-root',
  templateUrl: './instructors-root.component.html',
  styleUrls: ['./instructors-root.component.scss']
})
export class InstructorsRootComponent implements OnInit {
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Instructors')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .getItem();


  }
}
