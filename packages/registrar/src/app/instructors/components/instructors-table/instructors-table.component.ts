import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as INSTRUCTORS_LIST_CONFIG from './instructors-table.config.json';
import {AdvancedTableComponent} from '../../../tables/components/advanced-table/advanced-table.component';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-instructors-table',
  templateUrl: './instructors-table.component.html'
})
export class InstructorsTableComponent implements OnInit {

  public readonly config = INSTRUCTORS_LIST_CONFIG;

  @ViewChild('table') table: AdvancedTableComponent;

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {

  }

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  onSelectBoxChange(event: any) {
    // get value
    const value = parseInt((<HTMLSelectElement>event.target).value, 10);
    if (isNaN(value)) {
      this._router.navigate(
          [],
          {
            relativeTo: this._activatedRoute,
            queryParams: { $filter: '' },
            queryParamsHandling: 'merge'
          }).then(() => {
            this.table.fetch();
      });
      return;
    }
    this._router.navigate(
        [],
        {
          relativeTo: this._activatedRoute,
          queryParams: { $filter: `status eq ${value}` },
          queryParamsHandling: 'merge'
        }).then(() => {
      this.table.fetch();
    });
  }

}
