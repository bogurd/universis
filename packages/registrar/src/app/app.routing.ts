import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import {AuthModule, AuthGuard} from '@universis/common';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [
      AuthGuard
    ],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'courses',
        loadChildren: './courses/courses.module#CoursesModule'
      },
      {
        path: 'classes',
        loadChildren: './classes/classes.module#ClassesModule'
      },
      {
        path: 'exams',
        loadChildren: './exams/exams.module#ExamsModule'
      },
      {
        path: 'instructors',
        loadChildren: './instructors/instructors.module#InstructorsModule'
      },
      {
        path: 'students',
        loadChildren: './students/students.module#StudentsModule'
      },
      {
        path: 'study-programs',
        loadChildren: './study-programs/study-programs.module#StudyProgramsModule'
      },
      {
        path: 'requests',
        loadChildren: './requests/requests.module#RequestsModule'
      },
      {
        path: 'theses',
        loadChildren: './theses/theses.module#ThesesModule'
      },
      {
        path: 'scholarships',
        loadChildren: './scholarships/scholarships.module#ScholarshipsModule'
      },
      {
        path: 'internships',
        loadChildren: './internships/internships.module#InternshipsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
        paramsInheritanceStrategy: 'always'
      }), AuthModule],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
