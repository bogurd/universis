import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-courses-root',
  template: '<router-outlet></router-outlet>'
})
export class CoursesRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
