import { Component, OnInit } from '@angular/core';
import * as COURSES_LIST_CONFIG from './courses-table.config.json';

@Component({
  selector: 'app-courses-table',
  templateUrl: './courses-table.component.html',
  styles: []
})
export class CoursesTableComponent implements OnInit {

  public readonly config = COURSES_LIST_CONFIG;

  constructor() { }

  ngOnInit() {
  }

}
