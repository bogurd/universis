import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CoursesHomeComponent} from './components/courses-home/courses-home.component';
import {CoursesTableComponent} from './components/courses-table/courses-table.component';
import {CoursesRootComponent} from './components/courses-root/courses-root.component';
import {CoursesPreviewComponent} from './components/courses-preview/courses-preview.component';
import {CoursesPreviewGeneralComponent} from './components/courses-preview-general/courses-preview-general.component';

const routes: Routes = [
    {
        path: '',
        component: CoursesHomeComponent,
        data: {
            title: 'Courses'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'list'
          },
          {
            path: 'list',
            component: CoursesTableComponent,
            data: {
              title: 'Courses List'
            }
          },
          {
            path: 'active',
            component: CoursesTableComponent,
            data: {
              title: 'Active Courses'
            }
          }
        ]
    },
    {
      path: ':id',
      component: CoursesRootComponent,
      data: {
        title: 'Courses Home'
      },
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'preview'
        },
        {
          path: 'preview',
          component: CoursesPreviewComponent,
          data: {
            title: 'Courses Preview'
          },
          children: [
            {
              path: '',
              redirectTo: 'general'
            },
            {
              path: 'general',
              component: CoursesPreviewGeneralComponent,
              data: {
                title: 'Classes Preview General'
              }
            }
          ]
        }
      ]

  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class CoursesRoutingModule {
}
