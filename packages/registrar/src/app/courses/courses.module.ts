import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoursesSharedModule} from './courses.shared';
import {CoursesRoutingModule} from './courses.routing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { CoursesHomeComponent } from './components/courses-home/courses-home.component';
import { CoursesTableComponent } from './components/courses-table/courses-table.component';
import {environment} from '../../environments/environment';
import {AppSidebarService} from '../registrar-shared/services/app-sidebar.service';
import {TablesModule} from '../tables/tables.module';
import { CoursesPreviewComponent } from './components/courses-preview/courses-preview.component';
import { CoursesRootComponent } from './components/courses-root/courses-root.component';
import { CoursesPreviewGeneralComponent } from './components/courses-preview-general/courses-preview-general.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        CoursesSharedModule,
        CoursesRoutingModule,
        TablesModule,
        SharedModule,
        FormsModule
    ],
    declarations: [ CoursesHomeComponent, CoursesTableComponent, CoursesPreviewComponent, CoursesRootComponent, CoursesPreviewGeneralComponent ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoursesModule implements OnInit {

    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch(err => {
            console.error('An error occurred while loading courses module');
            console.error(err);
        });
    }

    async ngOnInit() {
        // create promises chain
        const sources = environment.languages.map(async (language) => {
            const translations = await import(`./i18n/courses.${language}.json`);
            this._translateService.setTranslation(language, translations, true);
        });
        // execute chain
        await Promise.all(sources);
    }

}
