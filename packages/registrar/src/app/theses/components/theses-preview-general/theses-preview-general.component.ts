import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-theses-preview-general',
  templateUrl: './theses-preview-general.component.html',
})
export class ThesesPreviewGeneralComponent implements OnInit {
  public model: any;
  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) {}

  async ngOnInit() {
    this.model = await this._context.model('StudentTheses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('student($expand=department,person),thesis($expand=instructor)')
      .getItem();
  }

}
