import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-theses-root',
  template: '<router-outlet></router-outlet>',
})
export class ThesesRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
