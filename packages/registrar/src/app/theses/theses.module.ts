import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThesesHomeComponent } from './components/theses-home/theses-home.component';
import {ThesesSharedModule} from './theses.shared';
import {ThesesRoutingModule} from './theses.routing';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {ThesesTableComponent} from './components/theses-table/theses-table.component';
import { ThesesPreviewComponent } from './components/theses-preview/theses-preview.component';
import { ThesesRootComponent } from './components/theses-root/theses-root.component';
import { ThesesPreviewGeneralComponent } from './components/theses-preview-general/theses-preview-general.component';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ThesesSharedModule,
    ThesesRoutingModule,
    TranslateModule,
    TablesModule,
    FormsModule,
    SharedModule
  ],
  declarations: [ThesesHomeComponent, ThesesTableComponent, ThesesPreviewComponent, ThesesRootComponent, ThesesPreviewGeneralComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ThesesModule {

  constructor(private _translateService: TranslateService) {
    //
  }
}
