import {template, at} from 'lodash';
import {TableColumnConfiguration} from './advanced-table.interfaces';
import {Injector} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';

export abstract class AdvancedColumnFormatter implements TableColumnConfiguration {

    protected injector: Injector;
    public class: string;
    public defaultContent: string;
    public formatString: string;
    public formatter: string;
    public hidden: boolean;
    public name: string;
    public order: string;
    public property: string;
    public sortable: boolean;
    public title: string;
    public formatOptions?: any;
    public actions?: Array<any>;

    abstract render(data: any, type: any, row: any, meta: any);
}


export class NestedPropertyFormatter extends AdvancedColumnFormatter {
    render(data, type, row, meta) {
        // get column
        const column = meta.settings.aoColumns[meta.col];
        if (column && column.data) {
            return at(row, column.data.replace(/\//g, '.'));
        }
        return data;
    }
    constructor() {
        super();
    }
}

export class LinkFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    if (column && column.data) {
      return `<a href="${template(this.formatString)(row)}">${data}</a>`;
    }
    return data;
  }
  constructor() {
    super();
  }
}

export class ActionLinkFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];

    if (column && column.data) {
        this.actions = this.actions || [];
        // return the dropdown menu with 3 dots
        return  template(`<div class='dropdown details-control' data-id='${data}' data-type='student' >
                  <a id="dropdownMenuButton-${data}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="fa-2x text-dots"></span></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton-${data}">
                     ${this.actions.map(x => `<a data-name='${x.name}' class="dropdown-item" href="${x.href}">
                         ${this.injector.get(TranslateService).instant(template(x.title)({value: data}))}
                        </a>`)
                      .join('')}
                    </div>
                </div>`)(row);
    }
    return data;
  }
  constructor() {
    super();
  }
}

export class TemplateFormatter extends AdvancedColumnFormatter {
    render(data, type, row, meta) {
        // get column
        const column = meta.settings.aoColumns[meta.col];
        if (column && column.data) {
            return template(this.formatString)(row);
        }
        return data;
    }
    constructor() {
        super();
    }
}

export class TrueFalseFormatter extends AdvancedColumnFormatter {

    render(data, type, row, meta) {
        return data ? this.injector.get(TranslateService).instant('TrueFalse.long.Yes') :
            this.injector.get(TranslateService).instant('TrueFalse.long.No');
    }

    constructor() {
        super();
    }
}

export class TranslationFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    if (column && column.data) {
      // console.log(data);
      return this.injector.get(TranslateService).instant(template(this.formatString)({value: data}));
    }
    return data;
  }
  constructor() {
    super();
  }
}


export class DateTimeFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    if (column && column.data) {
      const datePipe: DatePipe = new DatePipe(this.injector.get(TranslateService).currentLang);
      if (typeof data === 'string' && /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}$/.test(data)) {
        return datePipe.transform(new Date(data + ':00'), this.formatString);
      }
      return datePipe.transform(data, this.formatString);
    }
    return data;
  }
  constructor() {
    super();
  }
}

export class NgClassFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    if (column && column.data) {
      // get formatOptions
      if (this.formatOptions) {
        const ngClass = this.formatOptions.ngClass;
        // check condition for each key
        for (const key of Object.keys(ngClass)) {
          const result = template(ngClass[key])(row);
          const fn = new Function(`return ${result};`);
          // if condition returns true add class
          if (fn() === true) {
            return `<span class="${key}">${data}</span>`;
          }
        }
      }
      return data;
    }
    return data;
  }
  constructor() {
    super();
  }
}
