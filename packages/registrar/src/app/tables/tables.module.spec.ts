import {TestBed, async, inject} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {TablesModule} from './tables.module';
describe('InstructorsModule', () => {
  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot()
      ]
    }).compileComponents();
  }));
  it('should create an instance', inject([TranslateService], (translateService: TranslateService) => {
    const tablesModule = new TablesModule(translateService);
    expect(tablesModule).toBeTruthy();
  }));
});
