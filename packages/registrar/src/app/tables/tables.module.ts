import {InjectionToken, NgModule, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdvancedTableComponent, COLUMN_FORMATTERS} from './components/advanced-table/advanced-table.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {AdvancedTableSearchComponent} from './components/advanced-table/advanced-table-search.component';

import {
  ActionLinkFormatter,
  LinkFormatter,
  NestedPropertyFormatter,
  TemplateFormatter,
  TrueFalseFormatter,
  TranslationFormatter,
  DateTimeFormatter,
  NgClassFormatter
} from './components/advanced-table/advanced-table.formatters';


@NgModule({
    imports: [
        CommonModule,
        TranslateModule
    ],
    providers: [
      {
        provide: COLUMN_FORMATTERS, useValue: [
          NestedPropertyFormatter,
          LinkFormatter,
          TemplateFormatter,
          TrueFalseFormatter,
          ActionLinkFormatter,
          TranslationFormatter,
          DateTimeFormatter,
          NgClassFormatter
        ]
      }
      ],
    declarations: [AdvancedTableComponent, AdvancedTableSearchComponent],
    exports: [AdvancedTableComponent, AdvancedTableSearchComponent]
})
export class TablesModule implements OnInit {
    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch(err => {
            console.error('An error occurred while loading tables module');
            console.error(err);
        });
    }

    async ngOnInit() {
        environment.languages.forEach(language => {
            import(`./i18n/tables.${language}.json`).then((translations) => {
                this._translateService.setTranslation(language, translations, true);
            });
        });
    }

}
