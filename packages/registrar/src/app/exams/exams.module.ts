import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExamsHomeComponent } from './components/exams-home/exams-home.component';
import { ExamsRoutingModule } from './exams.routing';
import { ExamsSharedModule } from './exams.shared';
import { ExamsTableComponent } from './components/exams-table/exams-table.component';
import { ExamsPreviewComponent } from './components/exams-preview/exams-preview.component';
import { ExamsRootComponent } from './components/exams-root/exams-root.component';
import { TablesModule } from '../tables/tables.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@universis/common';
import { ExamsPreviewGeneralComponent } from './components/exams-preview-general/exams-preview-general.component';
import { ExamsPreviewStudentsComponent } from './components/exams-preview-students/exams-preview-students.component';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ExamsSharedModule,
    TablesModule,
    ExamsRoutingModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    ExamsHomeComponent,
    ExamsTableComponent,
    ExamsPreviewComponent,
    ExamsRootComponent,
    ExamsPreviewGeneralComponent,
    ExamsPreviewStudentsComponent]
  ,
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamsModule { }
