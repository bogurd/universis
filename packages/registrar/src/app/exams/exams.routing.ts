import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExamsHomeComponent } from './components/exams-home/exams-home.component';
import { ExamsTableComponent } from './components/exams-table/exams-table.component';
import { ExamsPreviewComponent } from './components/exams-preview/exams-preview.component';
import { ExamsRootComponent } from './components/exams-root/exams-root.component';
import { ExamsPreviewGeneralComponent } from './components/exams-preview-general/exams-preview-general.component';
import { ExamsPreviewStudentsComponent } from './components/exams-preview-students/exams-preview-students.component';

const routes: Routes = [
  {
    path: '',
    component: ExamsHomeComponent,
    data: {
      title: 'Exams'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: ExamsTableComponent,
        data: {
          title: 'Exams List'
        }
      }
    ]
  },
  {
    path: ':id',
    component: ExamsRootComponent,
    data: {
      title: 'Exam Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'preview'
      },
      {
        path: 'preview',
        component: ExamsPreviewComponent,
        data: {
          title: 'Exam Preview'
        },
        children: [
          {
            path: '',
            redirectTo: 'general'
          },
          {
            path: 'general',
            component: ExamsPreviewGeneralComponent,
            data: {
              title: 'Exams Preview General'
            }
          },
          {
            path: 'students',
            component: ExamsPreviewStudentsComponent,
            data: {
              title: 'Exams Preview Students'
            }
          }
        ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class ExamsRoutingModule {
}
