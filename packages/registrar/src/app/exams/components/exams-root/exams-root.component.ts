import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exams-root',
  template: '<router-outlet></router-outlet>',
})
export class ExamsRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
