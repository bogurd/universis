import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-exams-preview-general',
  templateUrl: './exams-preview-general.component.html',
  styleUrls: ['./exams-preview-general.component.scss']
})
export class ExamsPreviewGeneralComponent implements OnInit {
  public model: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('CourseExams')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('course,examPeriod,status,completedByUser,year')
      .getItem();
  }
}
