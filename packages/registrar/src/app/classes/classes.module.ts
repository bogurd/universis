import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassesHomeComponent } from './components/classes-home/classes-home.component';
import {ClassesTableComponent } from './components/classes-table/classes-table.component';
import { ClassesRootComponent } from './components/classes-root/classes-root.component';
import { ClassesPreviewComponent } from './components/classes-preview/classes-preview.component';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule} from '@ngx-translate/core';
import {ClassesRoutingModule} from './classes.routing';
import {ClasseSharedModule} from './classes.shared';
import { ClassesPreviewGeneralComponent } from './components/classes-preview-general/classes-preview-general.component';
import { ClassesPreviewStudentsComponent } from './components/classes-preview-students/classes-preview-students.component';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ClasseSharedModule,
    TablesModule,
    ClassesRoutingModule,
    SharedModule,
    FormsModule
  ],
  declarations: [ClassesHomeComponent,
    ClassesPreviewComponent,
    ClassesRootComponent,
    ClassesTableComponent,
    ClassesPreviewGeneralComponent,
    ClassesPreviewStudentsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ClassesModule { }
