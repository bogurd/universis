import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-classes-preview-general',
  templateUrl: './classes-preview-general.component.html'
})
export class ClassesPreviewGeneralComponent implements OnInit {

  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext
  ) { }

  async ngOnInit() {
    this.model = await this._context.model('CourseClasses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('period, status, course($expand=department)')
      .getItem();
    console.log(this.model);
  }


}
