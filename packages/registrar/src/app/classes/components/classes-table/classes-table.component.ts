import { Component, OnInit } from '@angular/core';
import * as CLASSES_LIST_CONFIG from './classes-table.config.json';

@Component({
  selector: 'app-classes-table',
  templateUrl: './classes-table.component.html',
  styles: []
})
export class ClassesTableComponent implements OnInit {

  public readonly config = CLASSES_LIST_CONFIG;

  constructor() { }

  ngOnInit() {
  }

}
