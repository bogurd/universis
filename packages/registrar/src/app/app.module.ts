import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import {AppComponent} from './app.component';
import {SharedModule, ConfigurationService} from '@universis/common';
import {AngularDataContext, DATA_CONTEXT_CONFIG} from '@themost/angular';
import {FullLayoutComponent} from './layouts/full-layout.component';
import {BreadcrumbsComponent} from './layouts/breadcrumb.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app.routing';
import {TranslateModule} from '@ngx-translate/core';
import {ErrorModule, AuthModule} from '@universis/common';
import {HashLocationStrategy, LocationStrategy, registerLocaleData} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {APP_LOCATIONS} from '@universis/common';
import {AppSidebarModule} from '@coreui/angular';

import * as locations from './app.locations';
import {RegistrarSharedModule} from './registrar-shared/registrar-shared.module';
import {BsDropdownModule} from 'ngx-bootstrap';
import {TablesModule} from './tables/tables.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [
        AppComponent,
        BreadcrumbsComponent,
        FullLayoutComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        TranslateModule.forRoot(),
        SharedModule.forRoot(),
        RegistrarSharedModule.forRoot(),
        RouterModule,
        AuthModule,
        FormsModule,
        AppRoutingModule,
        ErrorModule.forRoot(),
        AppSidebarModule,
        BsDropdownModule.forRoot(),
        TablesModule,

    ],
    providers: [
        {
            provide: DATA_CONTEXT_CONFIG, useValue: {
                base: '/',
                options: {
                    useMediaTypeExtensions: false,
                    useResponseConversion: true
                }
            }
        },
        {
            provide: APP_LOCATIONS, useValue: locations.REGISTRAR_APP_LOCATIONS
        },
        AngularDataContext,
        {
            provide: APP_INITIALIZER,
            // use APP_INITIALIZER to load application configuration
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                // load application configuration
                    return configurationService.load().then( loaded => {
                        // load angular locales
                        const sources = configurationService.settings.i18n.locales.map(locale => {
                           return import(`@angular/common/locales/${locale}.js`).then(module => {
                               // register locale data
                               registerLocaleData(module.default);
                               // return
                               return Promise.resolve();
                           });
                        });
                        return Promise.all(sources).then(() => {
                            // return true for APP_INITIALIZER
                            return Promise.resolve(true);
                        });
                    });
                },
            deps: [ ConfigurationService ],
            multi: true
        },
        {
            provide: LOCALE_ID,
            useFactory: (configurationService: ConfigurationService) => {
                return configurationService.currentLocale;
            },
            deps: [ConfigurationService]
        },
        {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
        }
    ],
    bootstrap: [ AppComponent ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ]
})
export class AppModule {

}
