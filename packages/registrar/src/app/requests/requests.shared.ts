import {NgModule, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateService} from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})

export class RequestsSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading requests shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/requests.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
