import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestsPreviewComponent } from './requests-preview.component';

describe('RequestsPreviewComponent', () => {
  let component: RequestsPreviewComponent;
  let fixture: ComponentFixture<RequestsPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
