import {Component, OnInit, ViewChild} from '@angular/core';
import {RequestsService} from '../../services/requests.service';
import {ActivatedRoute} from '@angular/router';
import {DIALOG_BUTTONS, ErrorService, LoadingService, ModalService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-requests-edit',
  templateUrl: './requests-edit.component.html',
  styleUrls: ['./requests-edit.component.scss']
})
export class RequestsEditComponent implements OnInit {

  public request: any;
  public showResponseForm = false;
  public showCancelButton = false;
  public showNewResponseButton = false;
  public requestid;
  public selectedFile: File;
  @ViewChild('fileinput') fileinput;

  public responseModel = {
    body: null,
    attachment: null
  };

  constructor(private _requestsService: RequestsService,
              private _route: ActivatedRoute,
              private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _translate: TranslateService) { }

  ngOnInit() {
    this._route.parent.params.subscribe(routeParams => {
      this.requestid = routeParams.id;
      this.loadData(this.requestid);
    });
  }

  // loads general data
  loadData(requestid) {
    this.responseModel.attachment = null;
    this.responseModel.body = null;

    this._loadingService.showLoading();
    this._requestsService.getRequestById(requestid).then((res) => {
      this.request = res;

      if (res === undefined) {
        this._loadingService.hideLoading();
        return ;
      }

      // checks if request is cancel
      if (this.request.actionStatus.alternateName === 'CancelledActionStatus') {
        this.showResponseForm = false;
        this.showCancelButton = false;
        this.showNewResponseButton = false;
      } else {

        // if there are messages , the form and cancel button will be hidden
        if (this.request.messages.length > 0) {
          this.showResponseForm = false;
          this.showCancelButton = false;
          this.showNewResponseButton = true;
        } else {
          // else the new response button will be hidden
          this.showResponseForm = true;
          this.showCancelButton = true;
          this.showNewResponseButton = false;
        }
      }
      this._loadingService.hideLoading();
    }, (err) => {
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
  }

  // sends a response and changes request status to completed
  completeAction() {
    if (this.requestid) {
      this._loadingService.showLoading();
      this._requestsService.sendResponse(this.request.student.id,
        this.requestid,
        this.responseModel).subscribe(res => {
        // complete the action
        this._requestsService.completeAction(this.requestid).then((resAction) => {
          this.request.actionStatus.alternateName = 'CompletedActionStatus';
          this.showResponseForm = false;

          // reloads requests
          this.loadData(this.requestid);
        }, (err) => {
          this._loadingService.hideLoading();
          return this._errorService.navigateToError(err);
        });

      }, (err) => {
        this._loadingService.hideLoading();
        return this._errorService.navigateToError(err);
      });
    }
  }

  // cancels the request and sent a message
  cancelAction() {
    if (this.requestid) {

      this._modalService.showDialog(this._translate.instant('Requests.Edit.ModalCancelTitle'),
        this._translate.instant('Requests.Edit.ModalCancelMessage'),
        DIALOG_BUTTONS.OkCancel
        ).then((response) => {

        if (response === 'ok') {
          this._loadingService.showLoading();

          this._requestsService.sendResponse(this.request.student.id,
            this.requestid,
            this.responseModel).subscribe(res => {
              this._requestsService.cancelAction(this.requestid).then((resAction) => {
                this.request.actionStatus.alternateName = 'CancelledActionStatus';
                this.showResponseForm = false;
                this.showCancelButton = false;
                this.showNewResponseButton = false;

                // reloads requests
                this.loadData(this.requestid);
                this._loadingService.hideLoading();
              }, (err) => {
                this._loadingService.hideLoading();
                return this._errorService.navigateToError(err);
              });

          }, (err) => {
            this._loadingService.hideLoading();
            return this._errorService.navigateToError(err);
          });
        }

      });
    }
  }

  // download the document file
  downloadFile(attachments) {
    this._requestsService.downloadFile(attachments);
  }

  // get the selected file
  onFileChanged(event) {
    this.responseModel.attachment = this.fileinput.nativeElement.files[0];
  }

  // reset the file of html input element
  reset() {
    this.fileinput.nativeElement.value = '';
  }

}
