import { Component, OnInit } from '@angular/core';
import * as REQUESTS_LIST_CONFIG from './requests-table.config.json';

@Component({
  selector: 'app-requests-table',
  templateUrl: './requests-table.component.html',
  styles: []
})
export class RequestsTableComponent implements OnInit {

  public readonly config = REQUESTS_LIST_CONFIG;

  constructor() { }

  ngOnInit() {
  }

}
