import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private _context: AngularDataContext,
              private _http: HttpClient) { }

  getRequestById(request) {
    return this._context.model('RequestDocumentActions')
      .asQueryable()
      .where('id').equal(request)
      .expand( 'object, student($expand=person, studentStatus, inscriptionYear),messages($orderby=dateCreated desc;$expand=attachments)')
      .getItem();
  }

  completeAction(requestid): any {
    return this._context.model(`RequestDocumentActions`)
      .save({'id': requestid, 'actionStatus':  {'alternateName': 'CompletedActionStatus'}});
  }


  cancelAction(requestid): any {
    return this._context.model(`RequestDocumentActions`)
      .save({'id': requestid, 'actionStatus':  {'alternateName': 'CancelledActionStatus'}});
  }

  sendResponse(studentId, actionId, responseModel) {

    const formData: FormData = new FormData();
    if (responseModel.attachment) {
      formData.append('attachment', responseModel.attachment, responseModel.attachment.name);
    }
    formData.append('action', actionId);
    formData.append('body', responseModel.body);

    // get context service headers
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`students/${studentId}/messages/send`);

    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    });
  }

  downloadFile(attachments) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachments[0].url.replace(/\\/g, '/').replace('/api', '');

    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {

        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachments[0].name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

}
