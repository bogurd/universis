import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { AngularDataContext } from '@themost/angular';
import { ConfigurationService } from '../../shared/services/configuration.service';

export interface AuthCallbackResponse {
  access_token: string;
  expires: string;
  refresh_token: string;
  token_type: string;
  scope: string;
}

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient,
    private _config: ConfigurationService,
    private _context: AngularDataContext) {
  }

    /**
     * Redirects user to OAuth2 server authorization URL. This operation initializes an implicit authorization flow.
     */
  authorize() {
    const settings = this._config.settings.auth;
    /* tslint:disable-next-line max-line-length */
    window.location.href = `${settings.authorizeURL}?response_type=token&client_id=${settings.oauth2.clientID}&scope=${settings.oauth2.scope}`;
  }


  /**
   * Handles OAuth2 server callback of an implicit authorization flow.
   * @param access_token
   * @returns {Promise<*>} - Returns an object which represents the authorized user.
   */
  async callback(access_token) {
      // get configuration settings
      const settings = this._config.settings.auth;
      const body = new HttpParams().set('access_token', access_token);
      const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
      // call users/me
      this._context.setBearerAuthorization(access_token);
      // get user
      const user = await this._context.model('users/me').asQueryable().expand('groups').getItem();
      if (user == null) {
          throw new Error('Unauthorized');
      }
      // assign token to user
      user.token = {
          access_token: access_token
      };
      // store user to storage
      sessionStorage.setItem('currentUser', JSON.stringify(user));
      // and finally return user
      return user;
  }

    /**
     * Performs user logout.
     * @returns {Promise<boolean>} - Returns true if user has been logged out succesfully. Otherwise returns false.
     */
  async logout() {
    if (typeof sessionStorage.getItem('currentUser') === 'string') {
      // get current user
      const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
      // get authentication settings
      const settings  = this._config.settings.auth;
      // clear session storage
      sessionStorage.clear();
      // redirect to logout uri
      window.location.href = settings.logoutURL;
      return true;
    }
    return false;
  }
}
