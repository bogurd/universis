import {Component, OnInit, Input, ElementRef} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

export enum DIALOG_BUTTONS {
  Ok = 1,
  Yes = 2,
  No = 4,
  Abort = 8,
  Retry = 16,
  Ignore = 32,
  Cancel = 64,
  YesNo = 6,
  AbortRetryIgnore = 54,
  OkCancel = 65,
  YesNoCancel = 70
}

@Component({
  selector: 'universis-dialog.modal',
  styles: [`
      .modal-footer {
        border-top: 0;
      }
      .modal-ok {
        //
      }
      .modal-ok-cancel {
        //
      }
      .modal-yes-no {
        //
      }
      .modal-yes-no-cancel {
        //
      }
      .modal-abort-ignore-retry {
        //
      }
  `
  ],
  template: `
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">{{title}}</h4>
        </div>
        <div class="modal-body" [innerHTML]="message"></div>
        <div class="modal-footer">
          <button type="button" *ngIf="bitwiseAnd(buttons, 1)"
                  (click)="hide('ok')" class="btn btn-primary btn-ok" [translate]="'OK'"></button>
          <button type="button" *ngIf="bitwiseAnd(buttons, 2)"
                  (click)="hide('yes')" class="btn btn-primary btn-yes" [translate]="'Yes'"></button>
          <button type="button" *ngIf="bitwiseAnd(buttons, 4)"
                  (click)="hide('no')" class="btn btn-default btn-no" [translate]="'No'"></button>
          <button type="button" *ngIf="bitwiseAnd(buttons, 8)"
                  (click)="hide('abort')" class="btn btn-danger btn-abort" [translate]="'Abort'"></button>
          <button type="button" *ngIf="bitwiseAnd(buttons, 16)"
                  (click)="hide('retry')" class="btn btn-default btn-retry" [translate]="'Retry'"></button>
          <button type="button" *ngIf="bitwiseAnd(buttons, 32)"
                  (click)="hide('ignore')" class="btn btn-default btn-ignore" [translate]="'Ignore'"></button>
          <button type="button" *ngIf="bitwiseAnd(buttons, 64)"
                  (click)="hide('cancel')"  class="btn btn-default btn-cancel" [translate]="'Cancel'"></button>
        </div>
      </div>
    </div>
  `
})
/**
 *
 * A modal dialog component with ok and cancel buttons
 * @export
 * @class DialogComponent
 */
export class DialogComponent implements OnInit {


  @Input() title: string;
  @Input() message: string;
  public buttons: DIALOG_BUTTONS = DIALOG_BUTTONS.Ok;
  private modalRef: any;

  constructor(private _element: ElementRef, private _translateService: TranslateService) {
    //
  }

  public bitwiseAnd(a, b) {
    return a & b;
  }

  /**
   * Shows modal dialog
   * @returns Promise<any>
   */
  show() {
    return new Promise((resolve, reject) => {
        if (this.modalRef) {
          this.modalRef.one('hide.bs.modal', (event) => {
            // get related target if any
            const result = this.modalRef.data('result');
            // return result
            return resolve(result);
          });
          this.modalRef.modal('show');
        } else {
          reject('Modal element may not be empty at this context');
        }
    });
  }

  /**
   * Hides modal dialog
   * @param value
   */
  hide(value: any) {
    this.modalRef.data('result', value);
    this.modalRef.modal('hide');
  }

  /**
   * Converts modal buttons classes modal-ok, modal-yes-no etc to dialog buttons
   * @param {Array<string>} classList
   */
  private classListToButtons(classList: Array<string>) {
    return classList.map( classListElement => {
      // maps each item to an array of matches (if match)
      return /^modal(-ok)?(-yes)?(-no)?(-abort)?(-retry)?(-ignore)?(-cancel)?/ig.exec(classListElement);
    }).filter(match => {
      // filter not matched elements
      return match != null;
    }).map( match => {
      // maps each match as an array of 2 ^ k results
      return match.map((item, k) => {
        if (item && k > 0) {
          return Math.pow(2, k - 1);
        }
        return 0;
      }).reduce( (a, b) => {
        // return a sum of results
        return a + b;
      });
    }).reduce( (a, b) => {
      // return a final sum of results
      return a | b;
    });
  }

  async ngOnInit() {
    // get element classes
    const classList = Array.from((<HTMLDivElement>this._element.nativeElement).classList);
    // get buttons from element class list
    const classListButtons = this.classListToButtons(classList);
    // if element has button classes (modal-ok, modal-yes-no etc)
    if (classListButtons) {
      // set dialog buttons
      this.buttons = classListButtons;
    }
    // dynamic import jQuery
    return import('jquery').then(jQuery => {
      // dynamic import of bootstrap modal
      return import('bootstrap/js/dist/modal.js').then(() => {
        // set modal element
        this.modalRef = jQuery(this._element.nativeElement);
        // initialize modal
        this.modalRef.modal({
          backdrop: 'static',
          focus: true,
          keyboard: false,
          show: false
        });
      });
    });
  }
}
