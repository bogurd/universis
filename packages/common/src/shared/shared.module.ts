import {CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {LocalizedDatePipe} from './pipes/localized-date.pipe';
import {MsgboxComponent} from './components/msgbox/msgbox.component';
import {DialogComponent} from './components/modal/dialog.component';
import {SpinnerComponent} from './components/modal/spinner.component';
import {APP_CONFIGURATION, ApplicationConfiguration, ConfigurationService} from './services/configuration.service';
import {LoadingService} from './services/loading.service';
import {HttpClientModule} from '@angular/common/http';
import {ToastComponent} from './components/modal/toast.component';
import {ToastService} from './services/toast.service';
import {ModalService} from './services/modal.service';
import {NgVarDirective} from './directives/ngvar.directive';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule,
  ],
  declarations: [
    LocalizedDatePipe,
    MsgboxComponent,
    DialogComponent,
    SpinnerComponent,
    ToastComponent,
    NgVarDirective],
  entryComponents: [
    DialogComponent,
    SpinnerComponent,
    ToastComponent
  ],
  providers: [
    {
      provide: APP_CONFIGURATION,
      useValue: <ApplicationConfiguration>{
        settings: {
          remote: {
            server: '/'
          },
          i18n: {
            locales: [ 'en' ],
            defaultLocale: 'en'
          }
        }
      }
    },
    ConfigurationService,
    LoadingService,
    ToastService
  ],
  exports: [
    LocalizedDatePipe,
    MsgboxComponent,
    DialogComponent,
    SpinnerComponent,
    ToastComponent,
    NgVarDirective],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class SharedModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        ConfigurationService,
        ModalService,
        ToastService,
        LoadingService
      ]
    };
  }
}
