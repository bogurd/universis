import {Inject, Injectable, InjectionToken, Injector} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { isDevMode } from '@angular/core';
import { DATA_CONTEXT_CONFIG } from '@themost/angular';


export declare interface ApplicationSettingsConfiguration {
    name?: string;
    image?: string;
    description?: string;
    thumbnail?: string;
}

export declare interface RemoteSettingsConfiguration {
  server: string;
}

export declare interface LocalizationSettingsConfiguration {
  locales?: Array<string>;
  defaultLocale?: string;
    append?: any;
}

export declare interface SettingsConfiguration {
  app?: ApplicationSettingsConfiguration;
  remote: RemoteSettingsConfiguration;
  i18n?: LocalizationSettingsConfiguration;
  auth?: any;
}

export declare interface ApplicationConfiguration {
  settings: SettingsConfiguration;
}

export let APP_CONFIGURATION = new InjectionToken('app.configuration');


/**
 *
 * This Service is used to get or set global configuration for the project
 * @export
 * @class ConfigurationService
 */
@Injectable()
export class ConfigurationService {

  public config: any;
  constructor(private _translateService: TranslateService,
              private _injector: Injector,
              private _http: HttpClient) {
    //
  }

  /**
   *
   * Load Configs saved in Project
   * @returns {Promise<any>}
   * @memberof ConfigurationService
   */
  public async load(): Promise<boolean> {
    if (this.config) {
      return true;
    }
    // get environment
    const env = isDevMode() ? 'development' : 'production';
    // load configuration based on environment
    // e.g. assets/config/app.production.json or
    // assets/config/app.development.json
    return await this.loadFrom(`assets/config/app.${env}.json`);
  }

  public async loadFrom(url: string): Promise<boolean> {

    // get configuration from url
    this.config = await new Promise((resolve, reject) => {
      this._http.get(url).subscribe( result => {
        return resolve(result);
      }, err => {
        return reject(err);
      });
    });
    // get DATA_CONTEXT_CONFIG
    const dataContextConfig = this._injector.get(DATA_CONTEXT_CONFIG);
    // IMPORTANT: Set DATA_CONTEXT_CONFIG base URI from configuration
    dataContextConfig.base = this.config.settings.remote.server;
    // set locale for translate service
    this._translateService.use(this.currentLocale);
    // return
    return true;
  }

  /**
   * Gets current application settings
   */
  get settings(): SettingsConfiguration {
    return this.config.settings;
  }

  /**
   * Gets the current user language
   */
  get currentLocale(): string {
    const currentLang = localStorage.getItem('currentLang');
    if (currentLang) {
      return currentLang;
    }
    if (this.settings &&
        this.settings.i18n &&
        this.settings.i18n.defaultLocale) {
      // return current language
      return this.settings.i18n.defaultLocale;
    }
    // use fallback language
    return 'en';
  }

  set currentLocale(locale: string) {
    // save current locale
    localStorage.setItem('currentLang', locale);
    // set locale for translate service
    this._translateService.use(locale);
  }

}
