
// shared module
export { ConfigurationService,
    APP_CONFIGURATION,
    ApplicationConfiguration,
    ApplicationSettingsConfiguration,
    LocalizationSettingsConfiguration,
    RemoteSettingsConfiguration,
    SettingsConfiguration} from './shared/services/configuration.service';
export { ModalService } from './shared/services/modal.service';
export { DialogComponent, DIALOG_BUTTONS } from './shared/components/modal/dialog.component';
export { SpinnerComponent } from './shared/components/modal/spinner.component';
export { ToastComponent } from './shared/components/modal/toast.component';
export { ToastService } from './shared/services/toast.service';
export { LoadingService } from './shared/services/loading.service';
export { LocalizedDatePipe } from './shared/pipes/localized-date.pipe';
export { SharedModule } from './shared/shared.module';

// error module
export { ErrorBaseComponent, HttpErrorComponent } from './error/components/error-base/error-base.component';
export { ApiError, ProfileNotFoundError, RequestNotFoundError, UserProfileNotFoundError } from './error/error.custom';
export { ErrorsHandler } from './error/error.handler';
export { ErrorService } from './error/error.service';
export { ErrorRoutingModule } from './error/error.routing';
export { ErrorModule } from './error/error.module';

// auth module
export { LoginComponent } from './auth/components/login/login.component';
export { LogoutComponent } from './auth/components/logout/logout.component';
export { AuthGuard, APP_LOCATIONS, LocationPermission,
    LocationPermissionAccount, LocationPermissionTarget } from './auth/guards/auth.guard';
export { AuthRoutingModule } from './auth/auth.routing';
export { AuthCallbackComponent } from './auth/auth-callback.component';
export { AuthModule } from './auth/auth.module';
export { UserService } from './auth/services/user.service';
export { AuthCallbackResponse, AuthenticationService } from './auth/services/authentication.service';
