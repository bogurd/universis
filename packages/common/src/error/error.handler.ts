import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class ErrorsHandler implements ErrorHandler {

  handleError(error: Error) {
    console.error('ERROR', error);
    if (error instanceof HttpErrorResponse) {
      if (error.status === 401) {
        return ErrorsHandler.handlerUnauthorizedError(error);
      }
    }
  }

  static handlerUnauthorizedError(error) {
    window.location.href = `#/error/${error.status}.1?continue=/auth/loginAs`;
  }
}
