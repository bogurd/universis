import { ErrorHandler, ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import {
    ErrorBaseComponent,
    HttpErrorComponent
} from './components/error-base/error-base.component';
import { ErrorRoutingModule } from './error.routing';
import { ErrorService } from './error.service';
import { ErrorsHandler } from './error.handler';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        ErrorRoutingModule
    ],
    providers: [

    ],
    declarations: [
        ErrorBaseComponent,
        HttpErrorComponent
    ],
    exports: [
        ErrorBaseComponent,
        HttpErrorComponent
    ]
})
export class ErrorModule {

    constructor( @Optional() @SkipSelf() parentModule: ErrorModule) {
    }

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ErrorModule,
            providers: [
                ErrorService,
                {
                    provide: ErrorHandler,
                    useClass: ErrorsHandler,
                }
            ]
        };
    }


}
