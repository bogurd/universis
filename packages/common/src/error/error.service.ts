import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class ErrorService {

  private _lastError: any;

  constructor(private _router: Router) {
    //
  }

  /**
   * @param {*} error
   * @returns {Promise<boolean>}
   */
  navigateToError(error) {
    this.setLastError(error);
    return this._router.navigate(['/error']);
  }

  /**
   * Sets last application error
   * @param {*=} err
   * @return ErrorService
   */
  setLastError(err?: any) {
    this._lastError = err;
    return this;
  }

  /**
   * Gets last application error
   * @return {any}
   */
  getLastError() {
    return this._lastError;
  }

}
