import { ResponseError } from '@themost/client/common';

export class ApiError extends ResponseError {
  protected code: string;
  constructor(message: string, public statusCode: number) {
    super(message, statusCode);
    // important: set prototype for a class that extends Error in typescript
    this['__proto__'] = new.target.prototype;
    this.code = `E${statusCode}`;
  }
}

export class ProfileNotFoundError extends ApiError {

  public code: string;
  constructor() {
    super('User profile cannot be found', 404);
    // important: set prototype for a class that extends Error in typescript
    this['__proto__'] = new.target.prototype;
    this.code = 'E404.3';
  }
}

export class UserProfileNotFoundError extends ApiError {

  public code: string;
  constructor() {
    super('Your profile cannot be found', 404);
    // important: set prototype for a class that extends Error in typescript
    this['__proto__'] = new.target.prototype;
    this.code = 'E404.1';
  }
}

export class RequestNotFoundError extends ApiError {
  public code: string;
  constructor() {
    super('User request cannot be found', 404);
    // important: set prototype for a class that extends Error in typescript
    this['__proto__'] = new.target.prototype;
    this.code = 'E404.2';
  }
}
