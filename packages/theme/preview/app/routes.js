const routes = [
    {
        name: 'colors',
        title: 'Colors',
        url: '/colors',
        templateUrl: './app/colors.html'
    },
    {
        name: 'typography',
        title: 'Typography',
        url: '/typography',
        templateUrl: './app/typography.html'
    },
    {
        name: 'components',
        title: 'Components',
        url: '/components',
        templateUrl: './app/components.html',
        redirectTo: 'components.cards'
    },
    {
        name: 'components.cards',
        title: 'Cards',
        url: '/cards',
        templateUrl: './app/components/cards.html'
    },
    {
        name: 'components.group-lists',
        title: 'Group Lists',
        url: '/group-lists',
        templateUrl: './app/components/group-lists.html'
    },
    {
        name: 'components.buttons',
        title: 'Buttons',
        url: '/buttons',
        templateUrl: './app/components/buttons.html'
    },
    {
        name: 'forms',
        title: 'Forms',
        url: '/forms',
        templateUrl: './app/forms.html',
        redirectTo: 'forms.simple'
    },
    {
        name: 'forms.simple',
        title: 'Simple',
        url: '/simple',
        templateUrl: './app/forms/simple-form.html'
    },
    {
        name: 'forms.dropdown',
        title: 'Dropdown',
        url: '/dropdown',
        templateUrl: './app/forms/dropdown-form.html'
    },
    {
        name: 'lists',
        title: 'Lists',
        url: '/lists',
        templateUrl: './app/lists.html',
        redirectTo: 'lists.simple'
    },
    {
        name: 'lists.simple',
        title: 'List',
        url: '/list',
        templateUrl: './app/lists/simple-list.html'
    }
];

module.exports = routes;
