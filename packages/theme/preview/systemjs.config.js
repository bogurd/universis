// noinspection JSDeprecatedSymbols
System.config({
    paths: {
        'unpkg:*': '//unpkg.com/*'
    },
    baseURL: './',
    transpiler: 'plugin-babel',
    babelOptions: {
        sourceMaps: true,
        stage0: true
    },
    map: {
        'jquery':'unpkg:jquery@3.3.1/dist/jquery.js',
        'perfect-scrollbar':'unpkg:perfect-scrollbar@1.4.0/dist/perfect-scrollbar.common.js',
        'plugin-babel': 'unpkg:systemjs-plugin-babel@0/plugin-babel.js',
        'systemjs-babel-build': 'unpkg:systemjs-plugin-babel@0/systemjs-babel-browser.js',
        'angular': 'unpkg:angular@1.7.8/angular.min.js',
        '@uirouter/angularjs': 'unpkg:@uirouter/angularjs/release/angular-ui-router.min.js',
        '@coreui/coreui':'unpkg:@coreui/coreui@2.1.6/dist/js/coreui.js'
    }
});
